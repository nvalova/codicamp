<?php
	$page_title = 'Home';
	include('templates/header.php');
?>
<div class="slider">
  <div class="row-slider clearfix">
    <div class="hover-img">
      <img src="img/slider-post-1.png" alt="Post 1" width="307" height="74" />
    </div>
    <div class="hover-img">
      <img src="img/slider-post-2.png" alt="Post 2" width="314" height="74" />
    </div>
    <div class="hover-img">
      <img src="img/slider-post-3.png" alt="Post 3" width="307" height="74" />
    </div>
  </div>
  <div class="row-slider clearfix">
    <div class="hover-img">
      <img src="img/slider-post-4.png" alt="Post 4" width="627" height="298" />
    </div>
    <div class="hover-img">
      <img src="img/slider-post-5.png" alt="Post 5" width="307" height="183" class="img-slider-post" />
    </div>
    <div class="hover-img">
      <img src="img/slider-post-6.png" alt="Post 6" width="307" height="110" />
    </div>
  </div>
  <div class="row-slider clearfix">
    <div class="hover-img">
      <img src="img/slider-post-7.png" alt="Post 7" width="307" height="116" />
    </div>
    <div class="hover-img">
      <img src="img/slider-post-8.png" alt="Post 8" width="314" height="116" />
    </div>
    <div class="hover-img">
      <img src="img/slider-post-9.png" alt="Post 9" width="307" height="116" />
    </div>
  </div>
  <div class="slider-butons">
    <img src="img/arrow-left.png" class="arrow-left" />
    <img src="img/arrow-right.png" class="arrow-right" />
  </div>
  <div class="slider-info">
    <h2>Top iPhone Apps</h2>
    <p class="slider-text">Lorem ipsum dolor sit amet, ne utinam discere blandit vim, at iusto facilisis mel. Cetero audire sea an, has ex quem prima omnium.</p>
    <div>
      <a class="lnk-slider-more" href="/blog_item.php">More</a>
    </div>
  </div>
</div>
<section>
  <h1>A Theme Unlike Any Other. Simply Fantastic!</h1>
  <?php include('templates/grid-items.html'); ?>
</section>
<?php include('templates/pager.html'); ?>
<?php include('templates/footer.php'); ?>
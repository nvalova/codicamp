<?php
	$page_title = 'Home';
	include('templates/header.php');
?>
<h1>Sticker Mule. Best Place For Your Sticker Needs!</h1>
<div class="clearfix">
  <div class="left item-images">
    <img src="img/img-post-item-1.png" alt="Post item 1" width="540" height="400" />
    <img src="img/img-post-item-2.png" alt="Post item 2" width="540" height="400" />
  </div>
  <div class="right item-info">
    <div class="item-row clearfix">
      <div class="item-col">
        <p>Date</p>
        <p>April 15, 2012</p>
      </div>
      <div class="item-col">
        <p>Tags</p>
        <p>Website | Design</p>
      </div>
      <div class="item-col">
        <p>Author</p>
        <p>Michael Reimer</p>
      </div>
    </div>
    <p class="item-descripion">Lorem ipsum dolor sit amet, mollis epicuri pri ei, perpetua honestatis ad vix. Ne duo ludus putent, cu causae tamquam voluptua duo. Agam officiis no duo, ut reque decore sea. Cu eripuit accusam vix. Facete blandit detraxit pri cu, sea soluta doming civibus ea.</p>
    <p class="item-descripion-2">Eum eu tale clita iuvaret, cu est saperet forensibus interesset, cum ne case iusto oportere. Id idque indoctum eum, menandri mediocrem has ei. At usu modo quaerendum. Sit ei dicunt tacimates, mea ea enim eirmod suscipiantur, amet dicit ancillae vel in. Ex mea augue eloquentiam, his postea dolorem suavitate ea. Mel hendrerit accommodare concludaturque ex.</p>
    <div>
      <a class="link" href="blog_item.php">Visit Website</a>
    </div>
  </div>
</div>
<h1>Similar Posts. Check Them Out!</h1>
<div class="clearfix post-proposal-area">
  <article class="post-item">
    <div class="post-image hover-img">
      <img src="img/img-post-item-3.png" alt="Post 3" width="220" height="182" />
    </div>
    <div class="post-content">
      <h2>
        <a href="blog_item.php">Character Design</a>
      </h2>
      <div class="post-info">
        <span class="post-date">June 15, 2012</span>
      </div>
    </div>
  </article>
  <article class="post-item">
    <div class="post-image hover-img">
      <img src="img/img-post-item-4.png" alt="Post 4" width="220" height="182" />
    </div>
    <div class="post-content">
      <h2>
        <a href="blog_item.php">Top iPhone Apps</a>
      </h2>
      <div class="post-info">
        <span class="post-date">June 15, 2012</span>
      </div>
    </div>
  </article>
  <article class="post-item">
    <div class="post-image hover-img">
      <img src="img/img-post-item-5.png" alt="Post 5" width="220" height="182" />
    </div>
    <div class="post-content">
      <h2>
        <a href="blog_item.php">Social Media Buttons</a>
      </h2>
      <div class="post-info">
        <span class="post-date">June 15, 2012</span>
      </div>
    </div>
  </article>
  <article class="post-item">
    <div class="post-image hover-img">
      <img src="img/img-post-item-6.png" alt="Post 6" width="220" height="182" />
    </div>
    <div class="post-content">
      <h2>
        <a href="blog_item.php">10 Amazing Websites</a>
      </h2>
      <div class="post-info">
        <span class="post-date">June 15, 2012</span>
      </div>
    </div>
  </article>
</div>
<?php include('templates/footer.php'); ?>

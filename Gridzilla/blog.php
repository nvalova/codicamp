<?php
	$page_title = 'Home';
	include('templates/header.php');
?>

<section>
  <h1>Read Our Latest Posts. Learn Something New Maybe!</h1>
  <?php include('templates/grid-items.html'); ?>
</section>
<?php include('templates/pager.html'); ?>
<?php include('templates/footer.php'); ?>

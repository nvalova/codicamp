<footer class="clearfix">
  <div class="left social-icons-area">
    <a href="#">
      <img src="img/icon-twitter.png" alt="Twitter" width="39" height="37" />
    </a>
    <a href="#">
      <img src="img/icon-rss.png" alt="Rss" width="39" height="37" />
    </a>
    <a href="#">
      <img src="img/icon-facebook.png" alt="Facebook" width="39" height="37" />
    </a>
    <a href="#">
      <img src="img/icon-pinterest.png" alt="Pinterest" width="39" height="37" />
    </a>
  </div>
  <div class="right">
     <p class="info-email">
     	Best PSD Freebies  |  <a href="mailto:info@bestpsdfreebies.com">info@bestpsdfreebies.com</a>
     </p>
     <p class="info-copyright">
    	Copyright 2012 Gridzilla Theme  <a href="http://bestpsdfreebies.com">www.bestpsdfreebies.com</a>
     </p>
  </div>
</footer>
</div><!-- End of wrapper -->
    <script src="js/libs/jquery-1.8.3.min.js"></script>
    <script>
        if (typeof jQuery == 'undefined') {
            var e = document.createElement('script');
            e.src = "js/libs/jquery-1.8.3.min.js";
            e.type='text/javascript';
            document.getElementsByTagName("head")[0].appendChild(e);
        }
    </script>
    <script src="js/application.js"></script>
</body>
</html>
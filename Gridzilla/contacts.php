<?php
    $page_title = 'Contacts';
    include('templates/header.php');
?>
<h1>Contact Us. Locate Us. Send Us Your Thoughts!</h1>
<p class="intro-contact-page">
  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet. Duis sagittis ipsum. Praesent mauris. Maecenas in magna mollis lectus lacinia mollis.
</p>
<div id="map-canvas" style="height: 300px; width: 900px;"></div>
<form class="form-contact">
  <input type="text" placeholder="FIRST NAME" class="form-control" />
  <input type="text" placeholder="LAST NAME" class="form-control" />
  <input type="text" placeholder="EMAIL ADDRESS" class="form-control" />
  <textarea placeholder="MESSAGE" class="form-control text-message"></textarea>
  <button class="btn-submit" type="submit">Submit</button>
</form>

<?php include('templates/footer.php') ?>
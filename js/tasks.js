//Task 1
function max(a, b) {
    if (isNaN(a)) {
        console.error("a is not a number");
        return;
    }
    if (isNaN(b)) {
        console.error("b is not a number");
        return;
    }
    if (a > b) {
        return a;
    }
    else if (b > a) {
        return b;
    }
    else {
        return a;
    }
}

//Task 2
function maxOfThree(arg1, arg2, arg3) {
    if (isNaN(arg1)) {
        console.error("arg1 is not a number");
        return;
    }
    if (isNaN(arg2)) {
        console.error("arg2 is not a number");
        return;
    }
    if (isNaN(arg3)) {
        console.error("arg3 is not a number");
        return;
    }
}

//Task 3
function isVowel(letter) {
    if (letter.length == 1) {
        if (letter == "a" || letter == "e" || letter == "i" || letter == "o" || letter == "u") {
            return true;
        }
        else { return false; }
    }
    else {
        console.error("The length is more than 1.");
    }
}

//Task 4
function translate(str) {
    var result = '';
    var consonantArr = ['b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r', 's', 't', 'v', 'x', 'z'];
    for (var i = 0; i < str.length; i++) {
        if (consonantArr.indexOf(str[i].toLowerCase()) > -1) {
            result += str[i] + "o" + str[i];
        }
        else {
            result += str[i];
        }
    }
    return result;
}

//Task 5
function sum(arr) {
    var total = 0;
    for (var i = 0 ; i < arr.length; i++) {
        total += arr[i];
    }
    return total;
}

function multiply() {
    var total = 1;
    for (var i = 0 ; i < arr.length; i++) {
        total = total * arr[i];
    }
    return total;
}

//Task 6
function reverse(a) {
    var reversed = a.split('').reverse();
    return reversed.join('');
}

//Task 8
function findLongestWord(arr) {
    var current = 0;
    for (var i = 0; i < arr.length - 1; i++) {
        if (arr[i].length > current) {
            current = arr[i].length;
        }
    }
    return current;
}

//Task 9
function filterLongWords(arr, i) {
    var result = [];
    for (var j = 0; j < arr.length; j++) {
        if (i < arr[j].length) {
            result.push(arr[j]);
        }
    }
    return result;
}

//Task 11
function checkValue(amount) {
    if (amount != 1000000) {
        return amount + " leva";
    }
    else {
        return amount + " dollars ($$$)";
    }
}

//Task 12
// function mixUp(str1, str2) {
    // if (str1.length >= 2 && str2.length >= 2) {

    // }
    // else {
        // console.error("Strings are too short.");
        // return;
    // }
// }
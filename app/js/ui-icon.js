(function (GLOBAL) {
    /**
        
    * @param {String} uid
    */
    function UiIcon(uid) {
        this.uid = uid;
        this.elements = {};
    }

    /**
     * Initialize Icon
     */
    UiIcon.prototype.init = function() {
        console.info('Icon inited for window -> ', this.uid);
        this.render();
        this.attachEvents();
    };

    /**
     * Render dom markup
     */
    UiIcon.prototype.render = function() {
        this.elements.root = document.createElement('a');
        this.elements.root.href = 'javascript:;';
        
        this.elements.maximizeWindowButton = document.createElement('img');
        this.elements.maximizeWindowButton.src = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAABRUlEQVRoQ+2asQ6CQAyGy2P4DKKjj+Zk4sKij+ZodHPWxTCixgimKgkhd1CacoT438JyP9evvb8HaEQjH9HI4ycADF3BxgpMlvti6AB5/ct27o0TACEqhAqUWT5vZuKEp/eC4vXhM1+rKxczq4AkEHb9Ncvp8SJaJEcxgEsXHOCZE6W3nPjKQwrg0wUF4Ixz5qt9VwLQpAsGkD0L4j1fH20AbbogABw4B+IaTQASXa8AVdP5WpMLoIuuN4C66aQAXXW9ALhMJwHQ6MwBTknsNGsbgFZnDiA+ho0nmp3ExnGJb2cGsFtNxYtWT2KtznwLaQPR6gBQf6XUZlKrQwVQgd8LDbaQ1WcVbSa1OpgYJoaJvy7Aw1zZDbTdRKtDF0IXQhdCF8JvZJ0+Avgmmx1kJtEobvK/AIpkBZfgrwbBU15bcPQVeANkOMRAGJUkSAAAAABJRU5ErkJggg==";
        this.elements.maximizeWindowButton.setAttribute("height", "40");
        this.elements.maximizeWindowButton.setAttribute("width", "40");
        this.elements.maximizeWindowButton.className = "icon icons8-Closed-Window";

        this.elements.root.appendChild(this.elements.maximizeWindowButton);

        controller.elements.startLine.appendChild(this.elements.root);
    };

    UiIcon.prototype.destroy = function() {
        this.detachEvents();
        controller.elements.startLine.removeChild(this.elements.root);
    };

    /**
     * Bind Icon events
     */
    UiIcon.prototype.attachEvents = function () {
        this.destroy = this.destroy.bind(this);

        this.maximizeWindow = this.maximizeWindow.bind(this);

        Events.subscribe(window, 'window-removed-' + this.uid, this.destroy);
        Events.subscribe(this.elements.root, 'click', this.maximizeWindow);
    };

    /**
     * Remove Icon events
     */
    UiIcon.prototype.detachEvents = function () {
        Events.unsubscribe(window, 'window-removed-' + this.uid, this.destroy);
        Events.unsubscribe(this.elements.root, 'click', this.maximizeWindow);
    };

    UiIcon.prototype.maximizeWindow = function () {
        Events.publish(window, 'set-active-window', {
            uid: this.uid
        });
        console.log('toggle window state');
    };

    GLOBAL.UiIcon = UiIcon;
})(window);
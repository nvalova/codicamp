/**
 * Window goes Here
 * @param  {Object} GLOBAL
 * @return {Void}
 */
(function (GLOBAL){
    function UiWindow(uid) {
        this.uid = uid;
        this.elements = {};
    }

    UiWindow.prototype.init = function() {
        console.info('Window inited -> ', this.uid);

        this.render();
        this.attachEvents();
    };

    UiWindow.prototype.getElements = function () {
        this.elements.hideButton = this.elements.root.querySelector('.hide-button');
        this.elements.resizeButton = this.elements.root.querySelector('.resize-button');
        this.elements.closeButton = this.elements.root.querySelector('.close-button');
    };

    UiWindow.prototype.attachEvents = function () {

        this.destroy = this.destroy.bind(this);
        this.hide = this.hide.bind(this);
        this.resize = this.resize.bind(this);
        this.show = this.show.bind(this);

        Events.subscribe(this.elements.hideButton, 'click', this.hide);
        Events.subscribe(this.elements.resizeButton, 'click', this.resize);
        Events.subscribe(this.elements.closeButton, 'click', this.destroy);
        Events.subscribe(window, 'show-window-' + this.uid, this.show);
        Events.subscribe(window, 'hide-window-' + this.uid, this.hide);

    };

    UiWindow.prototype.detachEvents = function () {

        Events.unsubscribe(this.elements.hideButton, 'click', this.hide);
        Events.unsubscribe(this.elements.resizeButton, 'click', this.resize);
        Events.unsubscribe(this.elements.closeButton, 'click', this.destroy);
        Events.unsubscribe(window, 'show-window-' + this.uid, this.show);
        Events.unsubscribe(window, 'hide-window-' + this.uid, this.hide);
    };

    UiWindow.prototype.render = function () {
        this.elements.root = document.createElement('div');
        this.elements.root.className = "window-wrapper clearfix";
        this.elements.root.innerHTML = controller.uiWindowTemplate.replace('{{uid}}', this.uid);

        controller.elements.windowsPlaceholder.appendChild(this.elements.root);
        this.getElements();
    };

    UiWindow.prototype.destroy = function () {
        this.detachEvents();
        this.elements.root.remove();

        Events.publish(window, 'window-removed-' + this.uid);
        Events.publish(window, 'window-removed', {
            uid: this.uid
        });
    };

    UiWindow.prototype.hide = function () {
        this.elements.root.classList.add("hidden");
    };

    UiWindow.prototype.show = function () {
        this.elements.root.classList.remove("hidden");
    };

    UiWindow.prototype.resize = function () {
        this.elements.root.classList.toggle("window-resize");            
    };

    GLOBAL.UiWindow = UiWindow;
})(window);
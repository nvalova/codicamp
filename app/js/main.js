/**
 * Controller goes Here
 * @param  {Object} GLOBAL
 * @return {Void}
 */

(function (GLOBAL){
    var controller = {};

    controller.uiWindowsStorage = {};

    controller.uiWindowTemplate = "";

    /**
     * Initialize the application
     */
    controller.init = function () {
        this.getElements();
        this.preloadResources();
        this.attachEvents();
    };

    /**
     * Dom Storage
     */
    controller.elements = {};

    controller.currentActiveUIWindow = null;

    controller.addWindow = function () {

        var objID = "uiw_" + (new Date()).getTime();

        this.uiWindowsStorage[objID] = {
            uiWindow: new UiWindow(objID),
            icon: new UiIcon(objID)
        };

        this.uiWindowsStorage[objID].uiWindow.init();
        this.uiWindowsStorage[objID].icon.init();

        this.setActiveWindow(objID);
    };

    /**
     * Removes Window from storage
     * @param {Custom Event Object} event
     * @return {Void}
     */
    controller.removeWindow = function (event) {

        this.uiWindowsStorage[event.detail.uid] = null;
    };

    controller.onSetActiveWindow = function(event) {
        this.setActiveWindow(event.detail.uid);
    };

    controller.setActiveWindow = function(objID) {

        if (controller.currentActiveUIWindow != null) //previous active window
        {
            Events.publish(window, "hide-window-" + controller.currentActiveUIWindow.uiWindow.uid);
        }

        controller.currentActiveUIWindow = this.uiWindowsStorage[objID];
        Events.publish(window, "show-window-" + objID);
    };

    /**
     * Gets the window template via http request
    */
    controller.preloadResources = function () {
        var xmlhttpRequest = new XMLHttpRequest();
        
        xmlhttpRequest.onreadystatechange = function ajaxReadyState() {
            if (xmlhttpRequest.readyState === XMLHttpRequest.DONE) {
                if (xmlhttpRequest.status === 200) {
                    _success(xmlhttpRequest.response);
                }
                else {
                    _error(xmlhttpRequest.response);
                }
            }
        };
        xmlhttpRequest.open("GET", "templates/window-template.html", true);
        xmlhttpRequest.send();
    };

    function _success(response){
        controller.elements.createWindowButton.classList.remove("hidden");
        controller.uiWindowTemplate = response;
    }

    function _error(response){
        alert("Could not get the window template.");
        console.error(response);
    }

    /**
     * Store reference to Dom elements we will use
     */
    controller.getElements = function () {
        this.elements.windowsPlaceholder = document.getElementById('windows-placeholder');
        this.elements.createWindowButton = document.getElementById('create-window-button');
        this.elements.startLine = document.querySelector("#ui-placeholder .start-line");
    };

    /**
     * Bind global UI specific events
     */
    controller.attachEvents = function () {

        this.addWindow = this.addWindow.bind(this);
        this.removeWindow = this.removeWindow.bind(this);
        this.onSetActiveWindow = this.onSetActiveWindow.bind(this);

        Events.subscribe(this.elements.createWindowButton, 'click', this.addWindow);
        Events.subscribe(window, 'window-removed', this.removeWindow);
        Events.subscribe(window, 'set-active-window', this.onSetActiveWindow);
    };

    controller.init();
    GLOBAL.controller = controller;
})(window);
var Board = (function () {
    function init() {
        document.getElementById("btnAdd").addEventListener("click", function (event) {
            addNewParticipant();
        }, false);
    }

    function addNewParticipant() {
        var elName = document.getElementById("txtName");
        if (elName != undefined) {
            var name = elName.value;
            ScoreBoard.addParticipant(name);
            var elBody = document.getElementById("tblScoreBoard").getElementsByTagName("tbody")[0];
            var newRowIndex = elBody.rows.length;
            var row = elBody.insertRow(newRowIndex);
            row.insertCell(0).innerHTML = name;
            row.insertCell(1).innerHTML = 0;
            row.insertCell(2).innerHTML = 0;
            var actionCell = row.insertCell(3);
            actionCell.innerHTML = "<input type='number' class='numAddPoints' /><input type='button' class='btnAddPoints btn' value='Add Points' /><input type='button' class='btnDeleteParticipant btn' value='Delete' />";
            actionCell.getElementsByClassName("btnDeleteParticipant")[0].onclick = function deleteParticipant() {
                deleteParticipantByName(name, row);
            };
            actionCell.getElementsByClassName("btnAddPoints")[0].onclick = function addPoints() {
                addPointsToParticipant(name, row);
            };
        }
    }

    function addPointsToParticipant(name, row) {
        var pointsVal = row.getElementsByClassName("numAddPoints")[0].value;
        var points = parseInt(pointsVal);
        if (isNaN(points) == false) {
            ScoreBoard.givePoints(name, points);
            var participant = ScoreBoard.getParticipant(name);
            row.cells[1].innerHTML = participant.scores;
            row.cells[2].innerHTML = participant.beers;
        }
    }

    function deleteParticipantByName(name, row) {
        ScoreBoard.deleteParticipant(name);
        row.remove();
    }

    var boardApi = {
        init: init,
    };

    return boardApi;

})();

document.addEventListener("DOMContentLoaded", function documentReady() {
    Board.init();
});



